# My Portfolio

All the code to make my single page app!

![alt text](./public/images/000_Thumbnail.jpg "Logo Title Text 1")

## Prerequisites

This project is run through Node.js and Yarn:

- [Download and install Node.js](https://nodejs.org/)
- [Download and install Yarn](https://classic.yarnpkg.com/en/docs/install)

## Getting started

1. Clone repository from bitbucket:
```sh 
    git clone https://davide-bruno@bitbucket.org/davide-bruno/my-portfolio.git
``` 
2. Install node dependencies with Yarn
```sh 
    yarn install
``` 
3. Start the app locally
```sh 
    yarn start
``` 

## Deployment

The single page app get's deployed to a different bitbucket repository, which is responsible to host the necessary files to https://davide-bruno.bitbucket.io/

## Built With

* [Create React App](https://reactjs.org/docs/create-a-new-react-app.html#create-react-app) - A bootstrap service to kickstart the repository.
* [Material UI](https://material-ui.com/) - Main supporting dependency for styled components.
* [Jest](https://jestjs.io/) - Used to unit-test various functionalities
* [React Testing Library](https://testing-library.com/docs/react-testing-library/intro) - Used to unit-test react components

## Authors

* **Davide Bruno** - *Main developer*