function createMockWindowCallbacks() {
  const originalWindow = { ...window };

  function mockWindow(mock: Partial<Window>) {
    Object.assign(window, mock);
  }

  function resetWindow() {
    mockWindow(originalWindow);
  }

  return {
    beforeAll: resetWindow,
    afterEach: resetWindow,
    afterAll: resetWindow,
    mockWindow: mockWindow,
  };
}

export default createMockWindowCallbacks;
