import React from 'react';

type ArgsWithProps<PROPS extends {}> = [React.ComponentType<PROPS>, PROPS];
type ArgsWithoutProps<PROPS extends {}> = [React.ComponentType<PROPS>];

type CreateMockComponentArgs<PROPS extends {}> = {} extends PROPS
  ? ArgsWithProps<PROPS> | ArgsWithoutProps<PROPS>
  : ArgsWithProps<PROPS>;

function createMockComponent<PROPS extends {}>(
  ...args: CreateMockComponentArgs<PROPS>
): React.FunctionComponent<Partial<PROPS>> {
  const [Component, defaultProps = {} as PROPS] = args;
  return function mockComponent(customProps) {
    return <Component {...defaultProps} {...customProps} />;
  };
}

export default createMockComponent;
