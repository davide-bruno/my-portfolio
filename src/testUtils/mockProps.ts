function createMockProps<PROPS extends {}>(defaultProps: PROPS) {
  return function mockProps(customProps: Partial<PROPS> = {}) {
    return { ...defaultProps, ...customProps };
  };
}

export default createMockProps;
