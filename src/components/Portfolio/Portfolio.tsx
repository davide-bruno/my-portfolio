import {
  ThemeProvider,
  createMuiTheme,
  CssBaseline,
  WithWidthProps,
  withWidth,
} from '@material-ui/core';
import React, { useMemo, useState, useEffect } from 'react';
import PortfolioTopBar from 'components/PortfolioTopBar';
import PortfolioContainer from 'components/PortfolioContainer';
import PortfolioContent from 'components/PortfolioContent';
import { topBarHeight, mainGridId } from 'settings/miscellaneous.json';
import isDesktopLayout from 'utils/isDesktopLayout/isDesktopLayout';

const createTheme = (dark: boolean) =>
  createMuiTheme({
    palette: {
      type: dark ? 'dark' : 'light',
      primary: { main: '#657dff' },
    },
    overrides: {
      MuiCssBaseline: {
        '@global': {
          html: { scrollBehavior: 'smooth' },
          body: {
            backgroundColor: dark ? '#303030' : '#dadada',
          },
          [`#${mainGridId} :target:before`]: {
            content: '""',
            display: 'block',
            height: `${topBarHeight}px`,
          },
          ':target': {
            marginTop: `-${topBarHeight}px`,
          },
          '*': {
            transitionProperty: 'background-color, border-color',
            transitionDuration: '133ms',
            transitionTimingFunction: 'ease',
          },
        },
      },
      MuiAppBar: {
        colorPrimary: {
          backgroundColor: dark ? '#1f1f1f' : '#eee',
          color: dark ? '#fff' : '#000',
        },
      },
    },
  });

const Portfolio: React.FunctionComponent<WithWidthProps> = ({ width }) => {
  const [darkThemeActive, setDarkThemeActive] = useState(false);
  const [showMobileMenu, setShowMobileMenu] = useState(false);
  const theme = useMemo(() => createTheme(darkThemeActive), [darkThemeActive]);
  const layoutIsDesktop = useMemo(() => isDesktopLayout(width), [width]);

  useEffect(() => {
    if (layoutIsDesktop && showMobileMenu) setShowMobileMenu(false);
  }, [showMobileMenu, layoutIsDesktop]);

  const toggleShowMobileMenu = () => setShowMobileMenu((show) => !show);
  const menuIconOnClick = !layoutIsDesktop ? toggleShowMobileMenu : undefined;

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <PortfolioTopBar
        darkThemeActive={darkThemeActive}
        setDarkThemeActive={setDarkThemeActive}
        disableMenuIcon={layoutIsDesktop}
        menuIconOnClick={menuIconOnClick}
      />
      <PortfolioContainer>
        <PortfolioContent
          showMobileMenu={showMobileMenu}
          toggleShowMobileMenu={toggleShowMobileMenu}
        />
      </PortfolioContainer>
    </ThemeProvider>
  );
};

export default withWidth()(Portfolio);
