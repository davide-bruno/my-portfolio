import React from 'react';
import { render, waitFor } from '@testing-library/react';
import PortfolioSkillItem, {
  PortfolioSkillItemProps,
} from './PortfolioSkillItem';
import { PortfolioSkillsCardTestId } from './PortfolioSkillsCardConstants';
import createMockComponent from 'testUtils/mockComponent';
import { minSkill, maxSkill, skillRanks } from 'settings/skills.json';

const MockComponent = createMockComponent<PortfolioSkillItemProps>(
  (props) => (
    <table>
      <tbody>
        <PortfolioSkillItem {...props} />
      </tbody>
    </table>
  ),
  {
    title: 'Skill #1',
    skillValue: 3,
  }
);

test('renders an item', () => {
  const { queryByTestId } = render(<MockComponent />);
  const testId = PortfolioSkillsCardTestId.ITEM;
  const element = queryByTestId(testId);
  expect(element).toBeTruthy();
});

test('renders the title cell', () => {
  const { queryByTestId } = render(<MockComponent />);
  const testId = PortfolioSkillsCardTestId.ITEM_TITLE_CELL;
  const element = queryByTestId(testId);
  expect(element).toBeTruthy();
});

test('renders the title cell with the title attribute', () => {
  const title = 'My favourite skill';
  const { queryByTestId } = render(<MockComponent title={title} />);
  const testId = PortfolioSkillsCardTestId.ITEM_TITLE_CELL;
  const element = queryByTestId(testId);
  expect(element).toHaveAttribute('title', title);
});

test('renders the title', () => {
  const title = 'My favourite skill';
  const { queryByTestId } = render(<MockComponent title={title} />);
  const testId = PortfolioSkillsCardTestId.ITEM_TITLE_CELL;
  const element = queryByTestId(testId);
  expect(element).toHaveTextContent(title);
});

test('renders the progress bar cell', () => {
  const { queryByTestId } = render(<MockComponent />);
  const testId = PortfolioSkillsCardTestId.ITEM_PROGRESS_BAR_CELL;
  const element = queryByTestId(testId);
  expect(element).toBeTruthy();
});

test('should have a progress bar with min skillValue ', () => {
  const { queryByTestId } = render(<MockComponent skillValue={0} />);
  const testId = PortfolioSkillsCardTestId.ITEM_PROGRESS_BAR;
  const element = queryByTestId(testId);
  const expectedValue = `${(minSkill * 100) / maxSkill}`;

  expect(element).toHaveAttribute('aria-valuenow', expectedValue);
});

test('should have a progress bar with max skillValue ', () => {
  const skillValue = maxSkill * 10;
  const { queryByTestId } = render(<MockComponent skillValue={skillValue} />);
  const testId = PortfolioSkillsCardTestId.ITEM_PROGRESS_BAR;
  const element = queryByTestId(testId);
  expect(element).toHaveAttribute('aria-valuenow', '100');
});

test('should have a progress bar with the right title', () => {
  const skillValue = 4;
  const { queryByTestId } = render(<MockComponent skillValue={skillValue} />);
  const testId = PortfolioSkillsCardTestId.ITEM_PROGRESS_BAR_CELL;
  const element = queryByTestId(testId);
  const tooltip = skillRanks[skillValue - 1];
  expect(element).toHaveAttribute('title', tooltip);
});
