import {
  TableRow,
  TableCell,
  LinearProgress,
  Tooltip,
  makeStyles,
} from '@material-ui/core';
import React from 'react';
import { PortfolioSkillsCardTestId } from './PortfolioSkillsCardConstants';
import { minSkill, maxSkill, skillRanks } from 'settings/skills.json';
import mathClamp from 'utils/mathClamp';

const useStyles = makeStyles({
  titleCell: {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
  progressBarCell: {
    minWidth: '90px',
  },
});

export interface PortfolioSkillItemProps {
  title: string;
  skillValue: number;
}

const PortfolioSkillItem: React.FunctionComponent<PortfolioSkillItemProps> = ({
  title,
  skillValue,
}) => {
  const clampedSkillValue = mathClamp(skillValue, minSkill, maxSkill);
  const value = (clampedSkillValue * 100) / 5;
  const skillRank = skillRanks[clampedSkillValue - 1] || 'unknown';
  const classes = useStyles();

  return (
    <TableRow data-testid={PortfolioSkillsCardTestId.ITEM}>
      <Tooltip title={title} placement="left" arrow>
        <TableCell
          data-testid={PortfolioSkillsCardTestId.ITEM_TITLE_CELL}
          className={classes.titleCell}
          size="small"
        >
          {title}
        </TableCell>
      </Tooltip>
      <Tooltip title={skillRank} placement="right" arrow>
        <TableCell
          data-testid={PortfolioSkillsCardTestId.ITEM_PROGRESS_BAR_CELL}
          className={classes.progressBarCell}
        >
          <LinearProgress
            data-testid={PortfolioSkillsCardTestId.ITEM_PROGRESS_BAR}
            variant="determinate"
            value={value}
          />
        </TableCell>
      </Tooltip>
    </TableRow>
  );
};

export default PortfolioSkillItem;
