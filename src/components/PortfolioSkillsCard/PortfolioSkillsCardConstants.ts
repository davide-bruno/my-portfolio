export enum PortfolioSkillsCardTestId {
  TITLE = 'portfolioSkillsCardTitle',
  ITEM = 'portfolioSkillsCardItem',
  ITEM_TITLE_CELL = 'portfolioSkillsCardItemTitleCell',
  ITEM_PROGRESS_BAR_CELL = 'portfolioSkillsCardItemProgressBarCell',
  ITEM_PROGRESS_BAR = 'portfolioSkillsCardItemProgressBar',
}
