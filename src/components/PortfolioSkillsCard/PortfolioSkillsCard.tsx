import React from 'react';
import {
  TableContainer,
  TableBody,
  Table,
  makeStyles,
} from '@material-ui/core';
import uniqid from 'uniqid';
import BaseCard from 'components/BaseCard';
import PortfolioSkillItem, {
  PortfolioSkillItemProps,
} from './PortfolioSkillItem';
import { skillsTitle, skillsId } from 'settings/miscellaneous.json';
import { PortfolioSkillsCardTestId } from './PortfolioSkillsCardConstants';
import PortfolioCardHeader from 'components/PortfolioCardHeader';

const useStyles = makeStyles({
  table: {
    tableLayout: 'fixed',
  },
});

interface PortfolioSkillsCardProps {
  itemsProps?: PortfolioSkillItemProps[];
}

const PortfolioSkillsCard: React.FunctionComponent<PortfolioSkillsCardProps> = ({
  itemsProps = [],
}) => {
  const classes = useStyles();
  return (
    <TableContainer id={skillsId} component={BaseCard}>
      <PortfolioCardHeader data-testid={PortfolioSkillsCardTestId.TITLE}>
        {skillsTitle}
      </PortfolioCardHeader>
      <Table className={classes.table}>
        <TableBody>
          {itemsProps.map((props) => (
            <PortfolioSkillItem key={uniqid()} {...props} />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default PortfolioSkillsCard;
