import React from 'react';
import { render } from '@testing-library/react';
import PortfolioSkillsCard from './PortfolioSkillsCard';
import { PortfolioSkillsCardTestId } from './PortfolioSkillsCardConstants';
import { skillsTitle } from 'settings/miscellaneous.json';

test('renders title', () => {
  const { queryByTestId } = render(<PortfolioSkillsCard />);
  const element = queryByTestId(PortfolioSkillsCardTestId.TITLE);
  expect(element).toBeTruthy();
});

test('renders title text', () => {
  const { queryByTestId } = render(<PortfolioSkillsCard />);
  const element = queryByTestId(PortfolioSkillsCardTestId.TITLE);
  expect(element).toHaveTextContent(skillsTitle);
});

test('renders no items if no itemsProps is provided', () => {
  const { queryAllByTestId } = render(<PortfolioSkillsCard />);
  const elements = queryAllByTestId(PortfolioSkillsCardTestId.ITEM);
  expect(elements).toHaveLength(0);
});

test('renders as many items as provided in itemsProps', () => {
  const item = { title: 'My skill', skillValue: 5 };
  const items = [item, item, item];
  const { queryAllByTestId } = render(
    <PortfolioSkillsCard itemsProps={items} />
  );
  const elements = queryAllByTestId(PortfolioSkillsCardTestId.ITEM);
  expect(elements).toHaveLength(items.length);
});
