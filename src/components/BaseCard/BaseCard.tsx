import { Card, CardProps, Box } from '@material-ui/core';
import React from 'react';
import { ClassNameMap } from '@material-ui/core/styles/withStyles';

interface BaseCardProps extends Omit<CardProps, 'classes'> {
  classes?: Partial<ClassNameMap<'cardWrapper' | 'card'>>;
}

const BaseCard: React.FunctionComponent<BaseCardProps> = ({
  id,
  classes = {},
  ...props
}) => (
  <Box className={classes.cardWrapper} id={id} pt={2}>
    <Card className={classes.card} {...props} />
  </Box>
);

export default BaseCard;
