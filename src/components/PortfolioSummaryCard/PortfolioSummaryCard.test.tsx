import React from 'react';
import { render } from '@testing-library/react';
import PortfolioSummaryCard, {
  PortfolioSummaryCardProps,
} from './PortfolioSummaryCard';
import { PortfolioSummaryCardTestId } from './PortfolioSummaryCardConstants';
import createMockComponent from 'testUtils/mockComponent';

const MockComponent = createMockComponent<PortfolioSummaryCardProps>(
  PortfolioSummaryCard,
  {
    showMobileMenu: true,
    toggleShowMobileMenu: () => {},
    width: 'sm',
  }
);

test('renders no items if no itemsProps is provided', () => {
  const { queryAllByTestId } = render(<MockComponent />);
  const elements = queryAllByTestId(PortfolioSummaryCardTestId.ITEM);
  expect(elements).toHaveLength(0);
});

test('renders as many items as provided in itemsProps', () => {
  const items = [{}, {}, {}];
  const { queryAllByTestId } = render(<MockComponent itemsProps={items} />);
  const elements = queryAllByTestId(PortfolioSummaryCardTestId.ITEM);
  expect(elements).toHaveLength(items.length);
});
