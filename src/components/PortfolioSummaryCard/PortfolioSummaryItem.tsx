import {
  ListItem,
  ListItemText,
  Icon,
  Tooltip,
  makeStyles,
} from '@material-ui/core';
import React from 'react';
import { PortfolioSummaryCardTestId } from './PortfolioSummaryCardConstants';

const useStyles = makeStyles({
  icon: {
    width: '30px',
  },
  text: {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
});

export interface PortfolioSummaryItemProps {
  id?: string;
  title?: string;
  icon?: string;
  toggleShowMobileMenu?: () => void;
}

const PortfolioSummaryItem: React.FunctionComponent<PortfolioSummaryItemProps> = ({
  id = '',
  title = '',
  icon,
  toggleShowMobileMenu,
}) => {
  const classes = useStyles();
  return (
    <Tooltip title={title} placement="left" arrow>
      <ListItem
        data-testid={PortfolioSummaryCardTestId.ITEM}
        button
        component="a"
        href={`#${id}`}
        onClick={toggleShowMobileMenu}
        color="inherit"
      >
        {icon && (
          <Icon
            data-testid={PortfolioSummaryCardTestId.ITEM_ICON}
            className={classes.icon}
          >
            {icon}
          </Icon>
        )}
        <ListItemText
          data-testid={PortfolioSummaryCardTestId.ITEM_TEXT}
          disableTypography
          primary={title}
          className={classes.text}
        />
      </ListItem>
    </Tooltip>
  );
};

export default PortfolioSummaryItem;
