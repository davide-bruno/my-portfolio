import React from 'react';
import { render } from '@testing-library/react';
import PortfolioSummaryItem from './PortfolioSummaryItem';
import { PortfolioSummaryCardTestId } from './PortfolioSummaryCardConstants';

test('renders an item', () => {
  const { queryByTestId } = render(<PortfolioSummaryItem />);
  const element = queryByTestId(PortfolioSummaryCardTestId.ITEM);
  expect(element).toBeTruthy();
});

test('should have title attribute', () => {
  const title = 'I am an item!';
  const { queryByTestId } = render(<PortfolioSummaryItem title={title} />);
  const element = queryByTestId(PortfolioSummaryCardTestId.ITEM);
  expect(element).toHaveAttribute('title', title);
});

test('should render text component', () => {
  const title = 'I am an item!';
  const { queryByTestId } = render(<PortfolioSummaryItem title={title} />);
  const element = queryByTestId(PortfolioSummaryCardTestId.ITEM_TEXT);
  expect(element).toBeTruthy();
});

test('should render text component with the title as text', () => {
  const title = 'I am an item!';
  const { queryByTestId } = render(<PortfolioSummaryItem title={title} />);
  const element = queryByTestId(PortfolioSummaryCardTestId.ITEM_TEXT);
  expect(element).toHaveTextContent(title);
});

test('should anchor link to the specific id', () => {
  const id = 'special-element';
  const { queryByTestId } = render(<PortfolioSummaryItem id={id} />);
  const element = queryByTestId(PortfolioSummaryCardTestId.ITEM);
  expect(element).toHaveAttribute('href', `#${id}`);
});

test('should not render an icon if not specified', () => {
  const { queryByTestId } = render(<PortfolioSummaryItem />);
  const element = queryByTestId(PortfolioSummaryCardTestId.ITEM_ICON);
  expect(element).toBeNull();
});

test('should render an icon', () => {
  const icon = 'my-icon';
  const { queryByTestId } = render(<PortfolioSummaryItem icon={icon} />);
  const element = queryByTestId(PortfolioSummaryCardTestId.ITEM_ICON);
  expect(element).toHaveTextContent(icon);
});
