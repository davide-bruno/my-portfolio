import {
  List,
  Dialog,
  withWidth,
  WithWidthProps,
  makeStyles,
} from '@material-ui/core';
import uniqid from 'uniqid';
import React from 'react';
import PortfolioSummaryItem, {
  PortfolioSummaryItemProps,
} from './PortfolioSummaryItem';
import BaseCard from 'components/BaseCard';
import isDesktopLayout from 'utils/isDesktopLayout/isDesktopLayout';
import PortfolioCardHeader from 'components/PortfolioCardHeader';
import { summaryCardTitle } from 'settings/miscellaneous.json';

interface PortfolioSummaryContentProps {
  itemsProps?: PortfolioSummaryItemProps[];
  toggleShowMobileMenu: () => void;
}

const useStyles = makeStyles({
  list: {
    padding: '0px',
  },
});

const PortfolioSummaryContent: React.FunctionComponent<PortfolioSummaryContentProps> = ({
  itemsProps = [],
  toggleShowMobileMenu,
}) => {
  const classes = useStyles();

  return (
    <>
      <PortfolioCardHeader>{summaryCardTitle}</PortfolioCardHeader>
      <List className={classes.list}>
        {itemsProps.map((itemProps) => {
          const props = { ...itemProps, toggleShowMobileMenu };
          return <PortfolioSummaryItem key={uniqid()} {...props} />;
        })}
      </List>
    </>
  );
};

export interface PortfolioSummaryCardProps
  extends PortfolioSummaryContentProps,
    WithWidthProps {
  showMobileMenu: boolean;
}

const PortfolioSummaryCard: React.FunctionComponent<PortfolioSummaryCardProps> = ({
  width,
  showMobileMenu,
  toggleShowMobileMenu,
  itemsProps,
}) => {
  const contentProps = { itemsProps, toggleShowMobileMenu };
  const content = <PortfolioSummaryContent {...contentProps} />;

  return isDesktopLayout(width) ? (
    <BaseCard>{content}</BaseCard>
  ) : (
    <Dialog
      open={showMobileMenu}
      onClose={toggleShowMobileMenu}
      disableScrollLock
      disableEnforceFocus
    >
      {content}
    </Dialog>
  );
};

export default withWidth()(PortfolioSummaryCard);
