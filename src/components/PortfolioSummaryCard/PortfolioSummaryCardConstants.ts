export enum PortfolioSummaryCardTestId {
  ITEM = 'portfolioSideNavItem',
  ITEM_ICON = 'portfolioSideNavItemIcon',
  ITEM_TEXT = 'portfolioSideNavItemText',
}
