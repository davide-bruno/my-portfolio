import React from 'react';
import useStickyScrollTopStyle from 'hooks/useStickyScrollTopStyle';
import { topBarHeight, cardMargin } from 'settings/miscellaneous.json';
import { Grid, GridProps, makeStyles } from '@material-ui/core';

interface UseStylesOptions {
  top?: string;
}

const useStyles = makeStyles({
  root: ({ top }: UseStylesOptions) => ({ position: 'sticky', top }),
});

interface StickyGridColumnProps extends Omit<GridProps, 'item' | 'container'> {}

const StickyGridColumn: React.FunctionComponent<StickyGridColumnProps> = ({
  children,
  ...props
}) => {
  const { elementRef, topStyle: top } = useStickyScrollTopStyle<HTMLDivElement>(
    { topOffset: topBarHeight, bottomOffset: cardMargin }
  );
  const classes = useStyles({ top });

  return (
    <Grid item {...props}>
      <div ref={elementRef} className={classes.root}>
        <>{children}</>
      </div>
    </Grid>
  );
};

export default StickyGridColumn;
