import React from 'react';
import {
  PortfolioCardContentType,
  PortfolioCardContentTestId,
} from './PortfolioCardContentConstants';
import { isArray, isString } from 'util';
import uniqid from 'uniqid';
import { Link } from '@material-ui/core';

export interface PortfolioLinkProps {
  type: PortfolioCardContentType.LINK;
  content: string;
  href: string;
}

const PortfolioLink: React.FunctionComponent<PortfolioLinkProps> = ({
  content,
  href,
}) => (
  <Link
    href={href}
    target="_blank"
    data-testid={PortfolioCardContentTestId.LINK}
  >
    {content}
  </Link>
);

export interface PortfolioParagraphProps {
  type: PortfolioCardContentType.PARAGRAPH;
  content: string | (string | PortfolioLinkProps)[];
}

const PortfolioParagraph: React.FunctionComponent<PortfolioParagraphProps> = ({
  content,
}) => {
  if (isArray(content)) {
    return (
      <p data-testid={PortfolioCardContentTestId.PARAGRAPH}>
        {content.map((props) =>
          isString(props) ? props : <PortfolioLink key={uniqid()} {...props} />
        )}
      </p>
    );
  }

  return <p data-testid={PortfolioCardContentTestId.PARAGRAPH}>{content}</p>;
};

interface PortfolioCardContentProps {
  content: PortfolioParagraphProps | PortfolioParagraphProps[];
}

const PortfolioCardContent: React.FunctionComponent<PortfolioCardContentProps> = ({
  content,
}) => {
  if (isArray(content))
    return (
      <>
        {content.map((props) => (
          <PortfolioParagraph key={uniqid()} {...props} />
        ))}
      </>
    );

  return <PortfolioParagraph key={uniqid()} {...content} />;
};

export default PortfolioCardContent;
