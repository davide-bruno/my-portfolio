export enum PortfolioCardContentType {
  PARAGRAPH,
  LINK,
}

export enum PortfolioCardContentTestId {
  PARAGRAPH = 'portfolioCardContentParagraph',
  LINK = 'portfolioCardContentLink',
}
