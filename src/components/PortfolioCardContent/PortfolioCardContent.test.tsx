import React from 'react';
import { render, within } from '@testing-library/react';
import PortfolioCardContent, {
  PortfolioLinkProps,
  PortfolioParagraphProps,
} from './PortfolioCardContent';
import {
  PortfolioCardContentTestId,
  PortfolioCardContentType,
} from './PortfolioCardContentConstants';
import createMockProps from 'testUtils/mockProps';
import createMockComponent from 'testUtils/mockComponent';

const mockParagraphProps = createMockProps<PortfolioParagraphProps>({
  type: PortfolioCardContentType.PARAGRAPH,
  content: 'Hello world',
});

const mockLinkProps = createMockProps<PortfolioLinkProps>({
  type: PortfolioCardContentType.LINK,
  content: 'Hello world',
  href: 'https://hello.world',
});

const MockComponent = createMockComponent(PortfolioCardContent, {
  content: mockParagraphProps(),
});

test('renders a paragraph', () => {
  const { queryByTestId } = render(<MockComponent />);
  expect(queryByTestId(PortfolioCardContentTestId.PARAGRAPH)).toBeTruthy();
});

test('renders 5 paragraphs', () => {
  const oneParagraph = mockParagraphProps();
  const multipleParagraphs = [
    oneParagraph,
    oneParagraph,
    oneParagraph,
    oneParagraph,
    oneParagraph,
  ];
  const { queryAllByTestId } = render(
    <MockComponent content={multipleParagraphs} />
  );
  const elements = queryAllByTestId(PortfolioCardContentTestId.PARAGRAPH);
  expect(elements).toHaveLength(multipleParagraphs.length);
});

test('renders a link inside a paragraph', () => {
  const linkProps = mockLinkProps();
  const content = mockParagraphProps({ content: ['Say: ', linkProps] });
  const { queryByTestId } = render(<MockComponent content={content} />);
  const paragraphElement = queryByTestId(PortfolioCardContentTestId.PARAGRAPH);
  const linkElement =
    paragraphElement &&
    within(paragraphElement).queryByTestId(PortfolioCardContentTestId.LINK);
  expect(linkElement).toBeTruthy();
});
