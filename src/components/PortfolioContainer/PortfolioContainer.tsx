import { WithWidthProps, withWidth, Container } from '@material-ui/core';
import React from 'react';
import isDesktopLayout from 'utils/isDesktopLayout/isDesktopLayout';

const PortfolioContainer: React.FunctionComponent<WithWidthProps> = ({
  width,
  children,
}) => {
  const fixed = !isDesktopLayout(width);
  return (
    <Container fixed={fixed} maxWidth="lg">
      <>{children}</>
    </Container>
  );
};

export default withWidth()(PortfolioContainer);
