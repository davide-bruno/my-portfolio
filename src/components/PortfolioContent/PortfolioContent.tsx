import React, { useMemo } from 'react';
import uniqid from 'uniqid';
import cardsSettings from 'settings/cards.json';
import PortfolioCard from 'components/PortfolioCard';
import PortfolioSkillsCard from 'components/PortfolioSkillsCard';
import { Grid, WithWidthProps, withWidth } from '@material-ui/core';
import PortfolioSummaryCard from 'components/PortfolioSummaryCard';
import isDesktopLayout from 'utils/isDesktopLayout/isDesktopLayout';
import StickyGridColumn from 'components/StickyGridColumn';
import idMapToArray from 'utils/idMapToArray';
import { skillsList } from 'settings/skills.json';
import {
  skillsId,
  skillsIcon,
  skillsTitle,
  mainGridId,
} from 'settings/miscellaneous.json';
import { PortfolioSummaryItemProps } from 'components/PortfolioSummaryCard';

const cardSettingsArray = idMapToArray(cardsSettings);
const skillSummarySetting = {
  title: skillsTitle,
  icon: skillsIcon,
  id: skillsId,
};

interface PortfolioContentBaseProps {
  showMobileMenu: boolean;
  toggleShowMobileMenu: () => void;
  summaryProps: PortfolioSummaryItemProps[];
}

const PortfolioContentMobile: React.FunctionComponent<PortfolioContentBaseProps> = ({
  summaryProps,
  ...sideNavProps
}) => (
  <Grid container spacing={4}>
    <Grid id={mainGridId} item>
      {cardSettingsArray.map((props) => (
        <PortfolioCard key={uniqid()} {...props} />
      ))}
      <PortfolioSkillsCard itemsProps={skillsList} />
    </Grid>
    <PortfolioSummaryCard {...sideNavProps} itemsProps={summaryProps} />
  </Grid>
);

const PortfolioContentDesktop: React.FunctionComponent<PortfolioContentBaseProps> = ({
  summaryProps,
  ...sideNavProps
}) => (
  <Grid container spacing={4}>
    <StickyGridColumn xs={3}>
      <PortfolioSummaryCard {...sideNavProps} itemsProps={summaryProps} />
    </StickyGridColumn>
    <StickyGridColumn id={mainGridId} xs={6}>
      {cardSettingsArray.map((props) => (
        <PortfolioCard key={uniqid()} {...props} />
      ))}
    </StickyGridColumn>
    <StickyGridColumn xs={3}>
      <PortfolioSkillsCard itemsProps={skillsList} />
    </StickyGridColumn>
  </Grid>
);

interface PortfolioContentProps
  extends Omit<PortfolioContentBaseProps, 'summaryProps'>,
    WithWidthProps {
  showMobileMenu: boolean;
  toggleShowMobileMenu: () => void;
}

const PortfolioContent: React.FunctionComponent<PortfolioContentProps> = ({
  width,
  ...otherProps
}) => {
  const layoutIsDesktop = useMemo(() => isDesktopLayout(width), [width]);
  const summaryProps = useMemo(
    () =>
      layoutIsDesktop
        ? cardSettingsArray
        : [...cardSettingsArray, skillSummarySetting],
    [layoutIsDesktop]
  );
  const contentProps = { summaryProps, ...otherProps };

  return layoutIsDesktop ? (
    <PortfolioContentDesktop {...contentProps} />
  ) : (
    <PortfolioContentMobile {...contentProps} />
  );
};

export default withWidth()(PortfolioContent);
