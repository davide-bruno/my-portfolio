import React from 'react';
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Icon,
  makeStyles,
} from '@material-ui/core';
import PortfolioThemeSwitch from 'components/PortfolioThemeSwitch';
import PortfolioContainer from 'components/PortfolioContainer';
import { pageTitle, topBarHeight } from 'settings/miscellaneous.json';

const useStyles = makeStyles(({ spacing, breakpoints }) => ({
  root: {
    height: `${topBarHeight}px`,
  },
  menuButton: {
    marginRight: spacing(2),
  },
  title: {
    flexGrow: 1,
    [breakpoints.down('sm')]: {
      fontSize: '1rem',
    },
  },
}));

interface PortfolioTopBarProps {
  darkThemeActive: boolean;
  setDarkThemeActive: React.Dispatch<React.SetStateAction<boolean>>;
  disableMenuIcon: boolean;
  menuIconOnClick?: () => void;
}

const PortfolioTopBar: React.FunctionComponent<PortfolioTopBarProps> = ({
  darkThemeActive,
  setDarkThemeActive,
  menuIconOnClick,
  disableMenuIcon,
}) => {
  const classes = useStyles();
  return (
    <AppBar position="sticky">
      <PortfolioContainer>
        <Toolbar className={classes.root} disableGutters>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
            onClick={menuIconOnClick}
            disabled={disableMenuIcon}
          >
            <Icon>menu</Icon>
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            {pageTitle}
          </Typography>
          <PortfolioThemeSwitch
            checked={darkThemeActive}
            onSwitch={setDarkThemeActive}
          />
        </Toolbar>
      </PortfolioContainer>
    </AppBar>
  );
};

export default PortfolioTopBar;
