export enum PortfolioThemeTestId {
  LIGHT_ICON = 'portfolioThemeLightIcon',
  DARK_ICON = 'portfolioThemeDarkIcon',
}
