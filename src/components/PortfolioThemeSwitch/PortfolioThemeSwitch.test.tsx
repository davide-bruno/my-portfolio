import { render, fireEvent } from '@testing-library/react';
import PortfolioThemeSwitch from './PortfolioThemeSwitch';
import React from 'react';
import { PortfolioThemeTestId } from './PortfolioThemeConstants';

const getButton = (container: HTMLElement): HTMLButtonElement | null =>
  container.querySelector('button');

test('renders a button', () => {
  const { container } = render(<PortfolioThemeSwitch />);
  expect(getButton(container)).toBeTruthy();
});

test('has Light Theme title', () => {
  const { container } = render(<PortfolioThemeSwitch />);
  expect(getButton(container)).toHaveAttribute('title', 'Light Theme');
});

test('has Dark Theme title', () => {
  const { container } = render(<PortfolioThemeSwitch checked />);
  expect(getButton(container)).toHaveAttribute('title', 'Dark Theme');
});

test('renders Light Theme icon', () => {
  const { queryByTestId } = render(<PortfolioThemeSwitch />);
  expect(queryByTestId(PortfolioThemeTestId.LIGHT_ICON)).toBeTruthy();
});

test('renders Dark Theme icon', () => {
  const { queryByTestId } = render(<PortfolioThemeSwitch checked />);
  expect(queryByTestId(PortfolioThemeTestId.DARK_ICON)).toBeTruthy();
});

test('does not run onSwitch function', () => {
  const onSwitch = jest.fn();
  render(<PortfolioThemeSwitch onSwitch={onSwitch} />);
  expect(onSwitch).not.toHaveBeenCalled();
});

test('does run onSwitch function once', () => {
  const onSwitch = jest.fn();
  const { container } = render(<PortfolioThemeSwitch onSwitch={onSwitch} />);
  const checkbox = getButton(container)!;
  fireEvent.click(checkbox);
  expect(onSwitch).toHaveBeenCalledTimes(1);
});
