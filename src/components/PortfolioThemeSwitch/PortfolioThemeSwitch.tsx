import React from 'react';
import { makeStyles, IconButton, Tooltip } from '@material-ui/core';
import { WbSunnyTwoTone, NightsStayTwoTone } from '@material-ui/icons';
import { PortfolioThemeTestId } from './PortfolioThemeConstants';

interface PortfolioSwitchProps {
  checked?: boolean;
  onSwitch?: (checked: boolean) => void;
}

const useStyles = makeStyles(({ palette }) => ({
  root: {
    color: palette.type === 'light' ? '#ffa707' : '#6868ff',
  },
}));

const PortfolioThemeSwitch: React.FunctionComponent<PortfolioSwitchProps> = ({
  checked = false,
  onSwitch,
}) => {
  const classes = useStyles();
  const onChange = onSwitch && (() => onSwitch(!checked));

  const startIcon = checked ? (
    <NightsStayTwoTone data-testid={PortfolioThemeTestId.DARK_ICON} />
  ) : (
    <WbSunnyTwoTone data-testid={PortfolioThemeTestId.LIGHT_ICON} />
  );
  const title = checked ? 'Dark Theme' : 'Light Theme';

  return (
    <Tooltip title={title} placement="bottom" arrow>
      <IconButton disableRipple classes={classes} onClick={onChange}>
        {startIcon}
      </IconButton>
    </Tooltip>
  );
};

export default PortfolioThemeSwitch;
