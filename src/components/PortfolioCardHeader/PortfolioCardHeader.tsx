import React from 'react';
import {
  CardContent,
  Typography,
  makeStyles,
  TypographyProps,
} from '@material-ui/core';

const useStyles = makeStyles(({ palette }) => ({
  root: {
    borderBottomWidth: '1px',
    borderBottomStyle: 'solid',
    borderBottomColor: palette.type === 'light' ? '#e0e0e0' : '#515151',
  },
  title: { fontSize: '1rem' },
}));

interface PortfolioCardHeaderProps extends TypographyProps {}

const PortfolioCardHeader: React.FunctionComponent<PortfolioCardHeaderProps> = ({
  children,
  ...props
}) => {
  const classes = useStyles();
  return (
    <CardContent className={classes.root}>
      <Typography
        variant="h6"
        component="span"
        className={classes.title}
        {...props}
      >
        {children}
      </Typography>
    </CardContent>
  );
};

export default PortfolioCardHeader;
