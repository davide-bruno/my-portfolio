import React from 'react';
import { render } from '@testing-library/react';
import PortfolioCard from './PortfolioCard';
import { PortfolioCardTestId } from './PortfolioCardConstants';
import { PortfolioCardContentType } from 'components/PortfolioCardContent/PortfolioCardContentConstants';


test('renders cardMedia', () => {
  const { queryByTestId } = render(<PortfolioCard imageUrl="./myImage.jpg" />);
  expect(queryByTestId(PortfolioCardTestId.CARD_MEDIA)).toBeTruthy();
});

test("rendered cardMedia has the right imageUrl as it's background", () => {
  const imageUrl = './myImage.jpg';
  const { queryByTestId } = render(<PortfolioCard imageUrl={imageUrl} />);
  expect(queryByTestId(PortfolioCardTestId.CARD_MEDIA)).toHaveStyle(
    `background-image: url(${imageUrl})`
  );
});

test('does not render cardMedia, if no image url is provided', () => {
  const { queryByTestId } = render(<PortfolioCard />);
  expect(queryByTestId(PortfolioCardTestId.CARD_MEDIA)).toBeNull();
});

test('renders title', () => {
  const { queryByTestId } = render(<PortfolioCard title="Greetings!" />);
  expect(queryByTestId(PortfolioCardTestId.CARD_TITLE)).toBeTruthy();
});

test("renders title's text node", () => {
  const title = 'Greetings!';
  const { queryByText } = render(<PortfolioCard title={title} />);
  expect(queryByText(title)).toBeTruthy();
});

test('does not render title, if no title is provided', () => {
  const { queryByTestId } = render(<PortfolioCard />);
  expect(queryByTestId(PortfolioCardTestId.CARD_TITLE)).toBeNull();
});

test('renders content wrapper', () => {
  const content = [
    { type: PortfolioCardContentType.PARAGRAPH, text: 'Hello world!' },
  ];
  const { queryByTestId } = render(<PortfolioCard content={content} />);
  expect(queryByTestId(PortfolioCardTestId.CARD_CONTENT_WRAPPER)).toBeTruthy();
});

test('does not render content wrapper, if there is no content', () => {
  const { queryByTestId } = render(<PortfolioCard />);
  expect(queryByTestId(PortfolioCardTestId.CARD_CONTENT_WRAPPER)).toBeNull();
});
