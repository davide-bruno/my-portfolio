import {
  CardContent,
  Typography,
  CardMedia,
  makeStyles,
  Tooltip,
} from '@material-ui/core';
import uniqid from 'uniqid';
import React from 'react';
import { PortfolioCardTestId } from './PortfolioCardConstants';
import { isPopulatedArray } from 'utils/typeGuards';
import PortfolioCardContent from 'components/PortfolioCardContent/PortfolioCardContent';
import BaseCard from 'components/BaseCard';
import { CSSProperties } from '@material-ui/core/styles/withStyles';

const useStyles = makeStyles({
  cardMedia: {
    height: '0px',
    paddingBottom: '50%',
  },
});

interface PortfolioCardProps {
  id?: string;
  title?: string;
  imageUrl?: string;
  imageY?: number;
  imageTitle?: string;
  content?: any[];
}

const getCardMediaInlineStyle = (
  imageY?: number
): CSSProperties | undefined => {
  if (imageY === undefined) return;
  return { backgroundPositionY: `${imageY}%` };
};

const PortfolioCard: React.FunctionComponent<PortfolioCardProps> = ({
  id,
  title,
  imageUrl,
  imageY,
  imageTitle = '',
  content = [],
}) => {
  const classes = useStyles();
  const cardMediaStyle = getCardMediaInlineStyle(imageY);
  return (
    <BaseCard id={id}>
      {imageUrl && (
        <Tooltip title={imageTitle} arrow>
          <CardMedia
            data-testid={PortfolioCardTestId.CARD_MEDIA}
            style={cardMediaStyle}
            className={classes.cardMedia}
            image={imageUrl}
          />
        </Tooltip>
      )}
      <CardContent>
        {title && (
          <Typography
            data-testid={PortfolioCardTestId.CARD_TITLE}
            gutterBottom
            variant="h5"
            component="h2"
          >
            {title}
          </Typography>
        )}
        {isPopulatedArray(content) && (
          <Typography
            data-testid={PortfolioCardTestId.CARD_CONTENT_WRAPPER}
            variant="body2"
            color="textSecondary"
            component="div"
          >
            <PortfolioCardContent key={uniqid()} content={content} />
          </Typography>
        )}
      </CardContent>
    </BaseCard>
  );
};

export default PortfolioCard;
