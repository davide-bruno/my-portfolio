export enum PortfolioCardTestId {
  CARD_MEDIA = 'cardMedia',
  CARD_TITLE = 'cardTitle',
  CARD_CONTENT_WRAPPER = 'cardContentWrapper',
}
