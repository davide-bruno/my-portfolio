import { useState, useCallback } from 'react';
import useEventListener, {
  UseEventListenerOptions,
} from 'hooks/useEventListener';
import { isNumber } from 'util';

interface ScrollDep {
  scrollY: number;
  prevScrollY: number;
  scrollYDiff: number;
}

type SetScrollYAction = React.SetStateAction<number>;
type SetScrollY = React.Dispatch<SetScrollYAction>;

const useScrollDep = (initialScrollY: number): [ScrollDep, SetScrollY] => {
  const [scrollDep, setScrollDep] = useState<ScrollDep>({
    scrollY: initialScrollY,
    prevScrollY: initialScrollY,
    scrollYDiff: 0,
  });

  const setScrollY = (newScrollY: SetScrollYAction) => {
    const scrollY = isNumber(newScrollY)
      ? newScrollY
      : newScrollY(scrollDep.scrollY);

    setScrollDep(({ scrollY: prevScrollY }) => ({
      scrollY,
      prevScrollY,
      scrollYDiff: scrollY - prevScrollY,
    }));
  };

  return [scrollDep, setScrollY];
};

export interface UseScrollListenerOptions
  extends Pick<UseEventListenerOptions, 'element' | 'dispatchAtStart'> {
  listener?: EventListener;
}

const useScrollListener = ({
  listener: scrollListener,
  ...options
}: UseScrollListenerOptions) => {
  const [scrollDep, setScrollY] = useScrollDep(window.scrollY);

  const listener: EventListener = useCallback((event) => {
    scrollListener && scrollListener(event);
    setScrollY(window.scrollY);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEventListener({ ...options, eventName: 'scroll', listener });

  return scrollDep;
};

export default useScrollListener;
