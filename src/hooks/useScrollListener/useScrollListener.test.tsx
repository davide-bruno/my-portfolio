import React from 'react';
import { render, act } from '@testing-library/react';
import useScrollListener, {
  UseScrollListenerOptions,
} from './useScrollListener';
import createMockWindowCallbacks from 'testUtils/mockWindow';
import createMockComponent from 'testUtils/mockComponent';

const mockComponentTestId = 'useScrollListenerMockComponentTestId';

const MockComponent = createMockComponent<UseScrollListenerOptions>(
  (options) => {
    const defaultOptions = {};
    const { scrollY, prevScrollY, scrollYDiff } = useScrollListener({
      ...defaultOptions,
      ...options,
    });

    return (
      <div
        data-testid={mockComponentTestId}
        data-scrolly={scrollY}
        data-prevscrolly={prevScrollY}
        data-scrollydiff={scrollYDiff}
      />
    );
  },
  { listener: () => {} }
);

const testCallbacks = createMockWindowCallbacks();

beforeAll(testCallbacks.beforeAll);
afterEach(testCallbacks.afterEach);
afterAll(testCallbacks.afterAll);

const dispatchEvent = () => {
  document.dispatchEvent(new Event('scroll'));
};

test('should call the listener once', () => {
  const listener = jest.fn();
  render(<MockComponent listener={listener} />);
  act(dispatchEvent);
  expect(listener).toHaveBeenCalledTimes(1);
});

test('should have basic scroll values', () => {
  const { getByTestId } = render(<MockComponent />);
  act(dispatchEvent);

  const element = getByTestId(mockComponentTestId);

  expect(element).toHaveAttribute('data-scrolly', '0');
  expect(element).toHaveAttribute('data-prevscrolly', '0');
  expect(element).toHaveAttribute('data-scrollydiff', '0');
});

test('should change scrollDep for each event dispatch', () => {
  const listener = jest.fn();
  const { getByTestId } = render(<MockComponent listener={listener} />);

  const scrollYValues = [4, 8, 12, 2, 7];

  scrollYValues.reduce((prevScrollY, scrollY, callIndex) => {
    act(() => {
      testCallbacks.mockWindow({ scrollY });
      dispatchEvent();
    });

    const element = getByTestId(mockComponentTestId);
    const scrollYDiff = scrollY - prevScrollY;

    expect(element).toHaveAttribute('data-scrolly', `${scrollY}`);
    expect(element).toHaveAttribute('data-prevscrolly', `${prevScrollY}`);
    expect(element).toHaveAttribute('data-scrollydiff', `${scrollYDiff}`);

    return scrollY;
  }, 0);
  expect(listener).toHaveBeenCalledTimes(scrollYValues.length);
});
