import { useEffect } from 'react';

export interface UseEventListenerOptions {
  eventName: string;
  listener: EventListener;
  element?: EventTarget;
  dispatchAtStart?: boolean;
}

function useEventListener({
  eventName,
  listener,
  element = document,
  dispatchAtStart = false,
}: UseEventListenerOptions) {
  useEffect(() => {
    element.addEventListener(eventName, listener);
    dispatchAtStart && element.dispatchEvent(new Event(eventName));
    return () => element.removeEventListener(eventName, listener);
  }, [dispatchAtStart, eventName, listener, element]);
}

export default useEventListener;
