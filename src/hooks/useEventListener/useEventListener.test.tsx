import useEventListener, { UseEventListenerOptions } from './useEventListener';
import React from 'react';
import { render } from '@testing-library/react';

const defaultEventName = 'scroll';

const MockComponent: React.FunctionComponent<Partial<
  UseEventListenerOptions
>> = (options) => {
  const defaultOptions = {
    eventName: defaultEventName,
    listener: () => {},
  };
  useEventListener({ ...defaultOptions, ...options });
  return <div />;
};

const dispatchEvent = (
  element: Node = document,
  eventName = defaultEventName
) => element.dispatchEvent(new Event(eventName));

test('should not call the listener at start', () => {
  const listener = jest.fn();
  render(<MockComponent listener={listener} />);
  expect(listener).not.toHaveBeenCalled();
});

test('should call the listener at start', () => {
  const listener = jest.fn();
  render(<MockComponent listener={listener} dispatchAtStart />);
  expect(listener).toHaveBeenCalled();
});

test('it should call the listener when the document element dispatches the event', () => {
  const listener = jest.fn();
  render(<MockComponent listener={listener} />);
  dispatchEvent();
  expect(listener).toHaveBeenCalledTimes(1);
});

test('it should call the listener as many times as the document element dispatches the event', () => {
  const listener = jest.fn();
  render(<MockComponent listener={listener} />);

  dispatchEvent();
  dispatchEvent();
  dispatchEvent();
  dispatchEvent();
  dispatchEvent();
  expect(listener).toHaveBeenCalledTimes(5);
});

test('it should call the listener when a specific element dispatches the event', () => {
  const element = document.createElement('div');
  const listener = jest.fn();
  render(<MockComponent listener={listener} element={element} />);

  dispatchEvent(element);

  expect(listener).toHaveBeenCalledTimes(1);
});

test('it should call the listener when a different element dispatches the event', () => {
  const element = document.createElement('div');
  const listener = jest.fn();
  render(<MockComponent listener={listener} element={element} />);

  dispatchEvent();

  expect(listener).not.toHaveBeenCalled();
});

test('it should not call the listener after it gets replaced', () => {
  const listener = jest.fn();
  const { rerender } = render(<MockComponent listener={listener} />);
  rerender(<MockComponent />);

  dispatchEvent();

  expect(listener).not.toHaveBeenCalled();
});
