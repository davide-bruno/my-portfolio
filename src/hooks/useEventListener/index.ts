import useEventListener from './useEventListener';
export * from './useEventListener';
export default useEventListener;
