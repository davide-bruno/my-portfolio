import React, { useEffect } from 'react';
import { render, act } from '@testing-library/react';
import useStickyScrollTopStyle, {
  UseStickyScrollStyleOptions,
} from './useStickyScrollTopStyle';
import createMockComponent from 'testUtils/mockComponent';
import createMockWindowCallbacks from 'testUtils/mockWindow';

const mockComponentTestId = 'useStickyScrollTopStyleMockComponentTestId';

interface MockComponentProps extends UseStickyScrollStyleOptions {
  height?: number;
}

const mockOffsetHeight = (element: HTMLDivElement, height = 0) =>
  jest.spyOn(element, 'offsetHeight', 'get').mockReturnValue(height);

const MockComponent = createMockComponent<MockComponentProps>(
  ({ height = 0, ...options }) => {
    const defaultOptions = {
      topOffset: 0,
      bottomOffset: 0,
    };
    const { elementRef, topStyle } = useStickyScrollTopStyle<HTMLDivElement>({
      ...defaultOptions,
      ...options,
    });
    useEffect(() => {
      if (elementRef.current) mockOffsetHeight(elementRef.current, height);
    }, [elementRef, height]);

    const style = { top: topStyle };
    return (
      <div data-testid={mockComponentTestId} ref={elementRef} style={style} />
    );
  },
  {
    topOffset: 0,
    bottomOffset: 0,
  }
);

const testCallbacks = createMockWindowCallbacks();

beforeAll(testCallbacks.beforeAll);
afterEach(testCallbacks.afterEach);
afterAll(testCallbacks.afterAll);

function dispatchEvent(eventName: string, element: EventTarget = document) {
  element.dispatchEvent(new Event(eventName));
}

function initializeWindowAction(windowMocks: Partial<Window>) {
  return () => {
    testCallbacks.mockWindow(windowMocks);
  };
}

function dispatchEventAction(
  eventName: string,
  element: EventTarget = document
) {
  return () => dispatchEvent(eventName, element);
}

function changeScrollYAction(scrollY: number) {
  return () => {
    testCallbacks.mockWindow({ scrollY });
    dispatchEvent('scroll');
  };
}

function changeInnerHeightAction(innerHeight: number) {
  return () => {
    testCallbacks.mockWindow({ innerHeight });
    dispatchEvent('resize', window);
  };
}

test('should have top to 0', () => {
  const { getByTestId } = render(<MockComponent />);
  expect(getByTestId(mockComponentTestId)).toHaveStyle('top: 0px');
});

test('should have top to 0 if there is nothing to scroll', () => {
  act(initializeWindowAction({ scrollY: 100, innerHeight: 50 }));
  const { getByTestId } = render(<MockComponent height={10} />);
  expect(getByTestId(mockComponentTestId)).toHaveStyle('top: 0px');
});

test('should scrollTop if the content is taller than the window', () => {
  act(initializeWindowAction({ scrollY: 0, innerHeight: 50 }));
  const { getByTestId } = render(<MockComponent height={200} />);
  act(changeScrollYAction(100));
  expect(getByTestId(mockComponentTestId)).toHaveStyle('top: -100px');
});

test('should limit itself to scroll to the bottom edge of the element', () => {
  act(initializeWindowAction({ scrollY: 0, innerHeight: 50 }));
  const { getByTestId } = render(<MockComponent height={200} />);
  act(changeScrollYAction(200));
  expect(getByTestId(mockComponentTestId)).toHaveStyle('top: -150px');
});

test('should start at the topOffset', () => {
  const { getByTestId } = render(
    <MockComponent topOffset={100} height={200} />
  );
  expect(getByTestId(mockComponentTestId)).toHaveStyle('top: 100px');
});

test('should consider the bottomOffset to limit the scroll', () => {
  act(initializeWindowAction({ scrollY: 0, innerHeight: 50 }));
  const { getByTestId } = render(
    <MockComponent topOffset={100} bottomOffset={10} height={200} />
  );
  act(changeScrollYAction(300));
  expect(getByTestId(mockComponentTestId)).toHaveStyle('top: -160px');
});

test('should listen to resize', () => {
  act(initializeWindowAction({ scrollY: 0, innerHeight: 50 }));
  const { getByTestId } = render(
    <MockComponent topOffset={100} bottomOffset={10} height={200} />
  );
  act(changeScrollYAction(300));
  expect(getByTestId(mockComponentTestId)).toHaveStyle('top: -160px');
  act(changeInnerHeightAction(100));
  expect(getByTestId(mockComponentTestId)).toHaveStyle('top: -110px');
});
