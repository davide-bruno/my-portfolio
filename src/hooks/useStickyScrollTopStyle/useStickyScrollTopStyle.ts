import { useRef, useState, useCallback, useEffect } from 'react';
import useScrollListener from 'hooks/useScrollListener';
import mathClamp from 'utils/mathClamp';
import useEventListener from 'hooks/useEventListener';

const useTopStyle = (initialTop: number) => {
  const [topStyle, setTopStyle] = useState<string>();
  const topRef = useRef(initialTop);

  const top = topRef.current;
  const setTop = useCallback((newTopValue: number) => {
    topRef.current = newTopValue;
    setTopStyle(`${newTopValue}px`);
  }, []);

  return { top, setTop, topStyle };
};

export interface UseStickyScrollStyleOptions {
  topOffset: number;
  bottomOffset: number;
}

const useStickyScrollTopStyle = <ELEMENT extends HTMLElement>({
  topOffset,
  bottomOffset,
}: UseStickyScrollStyleOptions) => {
  const elementRef = useRef<ELEMENT>(null);
  const [innerHeight, setInnerHeight] = useState(window.innerHeight);
  const { top, setTop, topStyle } = useTopStyle(topOffset);
  const scrollDep = useScrollListener({ dispatchAtStart: true });
  useEventListener({
    element: window,
    eventName: 'resize',
    listener: () => {
      setInnerHeight(window.innerHeight);
    },
  });

  useEffect(() => {
    if (!elementRef.current) return;
    const { scrollYDiff } = scrollDep;
    const { offsetHeight } = elementRef.current;
    const visibleHeight = offsetHeight + bottomOffset;
    const minTop = Math.min(topOffset, innerHeight - visibleHeight);
    const topValue = mathClamp(top - scrollYDiff, minTop, topOffset);
    setTop(topValue);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [scrollDep, innerHeight]);

  return { elementRef, topStyle };
};

export default useStickyScrollTopStyle;
