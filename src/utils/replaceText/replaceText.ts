const replaceText = (text: string, keys: Record<string, string> = {}) =>
  Object.entries(keys).reduce((currentText, [key, value]) => {
    const varName = `$${key}`;
    const regex = new RegExp(`((?<!_)\\${varName})`, 'g');
    const escapedRegex = new RegExp(`_\\${varName}`, 'g');
    return currentText.replace(regex, value).replace(escapedRegex, varName);
  }, text);

export default replaceText;
