import replaceText from './replaceText';

test('returns the same text', () => {
  const text = 'Hello World';

  const result = replaceText(text);

  expect(result).toBe('Hello World');
});

test('replaces specific key', () => {
  const text = 'Hello $name!';
  const name = 'Albert';

  const result = replaceText(text, { name });

  expect(result).toBe('Hello Albert!');
});

test('replaces multiple occurences of a specific key', () => {
  const text = 'Hello $name! Is $name actually your name?';
  const name = 'Albert';

  const result = replaceText(text, { name });

  expect(result).toBe('Hello Albert! Is Albert actually your name?');
});

test('does not replace keys if escaped', () => {
  const text = 'Hello $name! Is _$name actually your name?';
  const name = 'Albert';

  const result = replaceText(text, { name });

  expect(result).toBe('Hello Albert! Is $name actually your name?');
});

test('replaces multiple specific keys', () => {
  const text = 'Hello $name! My name is $secondName.';
  const name = 'Albert';
  const secondName = 'Thomas';

  const result = replaceText(text, { name, secondName });

  expect(result).toBe('Hello Albert! My name is Thomas.');
});
