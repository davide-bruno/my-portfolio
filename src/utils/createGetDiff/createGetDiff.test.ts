import createGetDiff from './createGetDiff';

test('should return a function', () => {
  expect(typeof createGetDiff()).toBe('function');
});

test('should return difference between initial value and next', () => {
  const getDiff = createGetDiff(5);
  expect(getDiff(0)).toBe(-5);
});

test('should initialize value to 0 if not provided', () => {
  const getDiff = createGetDiff();
  expect(getDiff(0)).toBe(0);
});

test('should consequently compare to previous value assigned', () => {
  const getDiff = createGetDiff();
  expect(getDiff(5)).toBe(5);
  expect(getDiff(10)).toBe(5);
  expect(getDiff(20)).toBe(10);
});
