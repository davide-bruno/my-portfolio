function createGetDiff(value = 0) {
  let currentValue = value;
  return function getDiff(nextValue: number) {
    const diff = nextValue - currentValue;
    currentValue = nextValue;
    return diff;
  };
}

export default createGetDiff;
