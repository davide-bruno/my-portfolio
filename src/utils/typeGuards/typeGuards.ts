export const isArray = <T>(value: T): value is Extract<T, any[]> =>
  Array.isArray(value);

export const isPopulatedArray = <T>(value: T): value is Extract<T, any[]> =>
  isArray(value) && !isEmptyArray(value);

export const isEmptyArray = <T>(value: T): value is Extract<T, []> =>
  isArray(value) && !value.length;

export const isNull = <T>(value: T): value is Extract<T, null> =>
  value === null;

export const isObject = <T>(
  value: T
): value is Exclude<Extract<T, {}>, any[]> =>
  !isArray(value) && !isNull(value) && typeof value === 'object';

export const isString = <T>(value: T): value is Extract<T, string> =>
  typeof value === 'string';

export const isNumber = <T>(value: T): value is Extract<T, number> =>
  typeof value === 'number';
