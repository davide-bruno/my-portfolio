import {
  isArray,
  isNull,
  isObject,
  isString,
  isEmptyArray,
  isPopulatedArray,
} from './typeGuards';
import { isNumber } from 'util';

test('variable is an array', () => {
  const result = isArray(['0']);

  expect(result).toBe(true);
});

test('variable is not an array', () => {
  const result = isArray(1);

  expect(result).toBe(false);
});

test('variable is a populated array', () => {
  const result = isPopulatedArray(['0']);

  expect(result).toBe(true);
});

test('variable is not a populated array', () => {
  const result = isPopulatedArray([]);

  expect(result).toBe(false);
});

test('variable is an empty array', () => {
  const result = isEmptyArray([]);

  expect(result).toBe(true);
});

test('variable is not an empty array', () => {
  const result = isEmptyArray(['0']);

  expect(result).toBe(false);
});

test('variable is null', () => {
  const result = isNull(null);

  expect(result).toBe(true);
});

test('variable is not null', () => {
  const result = isNull(1);

  expect(result).toBe(false);
});

test('variable is an object', () => {
  const result = isObject({ a: 'b' });

  expect(result).toBe(true);
});

test("variable is not an object if it's an array", () => {
  const result = isObject([1]);

  expect(result).toBe(false);
});

test("variable is not an object if it's null", () => {
  const result = isObject(null);

  expect(result).toBe(false);
});

test('variable is not an object', () => {
  const result = isObject('hello');

  expect(result).toBe(false);
});

test('variable is a string', () => {
  const result = isString('0');

  expect(result).toBe(true);
});

test('variable is a number', () => {
  const result = isNumber(0);

  expect(result).toBe(true);
});

test('variable is not an array', () => {
  const result = isString(1);

  expect(result).toBe(false);
});
