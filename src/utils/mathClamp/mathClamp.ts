const mathClamp = (value: number, min: number, max: number) => {
  if (min > max)
    throw new Error('MathClamp: min value is greater than the max value.');
  return Math.max(Math.min(value, max), min);
};
export default mathClamp;
