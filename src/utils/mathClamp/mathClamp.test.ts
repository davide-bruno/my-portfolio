import mathClamp from './mathClamp';

test('it should return the original value', () => {
  expect(mathClamp(5, 0, 10)).toBe(5);
});

test('it should return the min value', () => {
  expect(mathClamp(-10, 0, 10)).toBe(0);
});

test('it should return the max value', () => {
  expect(mathClamp(100, 0, 10)).toBe(10);
});

test('it should throw an error if the min value is greater than the max value', () => {
  expect(() => mathClamp(100, 10, 0)).toThrow();
});
