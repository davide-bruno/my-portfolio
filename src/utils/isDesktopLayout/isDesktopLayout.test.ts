import isDesktopLayout from './isDesktopLayout';

test('returns false if no width is provided', () => {
  expect(isDesktopLayout()).toBe(false);
});

test('returns false if width is xs', () => {
  expect(isDesktopLayout('xs')).toBe(false);
});

test('returns false if width is sm', () => {
  expect(isDesktopLayout('sm')).toBe(false);
});

test('returns true if width is md', () => {
  expect(isDesktopLayout('md')).toBe(true);
});

test('returns true if width is lg', () => {
  expect(isDesktopLayout('lg')).toBe(true);
});

test('returns true if width is xl', () => {
  expect(isDesktopLayout('xl')).toBe(true);
});
