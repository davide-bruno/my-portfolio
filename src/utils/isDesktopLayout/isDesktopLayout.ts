import { Breakpoint } from '@material-ui/core/styles/createBreakpoints';
import { isWidthUp } from '@material-ui/core';

const isDesktopLayout = (breakpoint: Breakpoint = 'xs') =>
  isWidthUp('md', breakpoint);

export default isDesktopLayout;
