import replaceText from 'utils/replaceText';
import { isString, isObject, isArray } from 'utils/typeGuards/typeGuards';

const initializeTexts = (
  textsObject: object,
  keys?: Record<string, string>
): object =>
  Object.entries(textsObject).reduce(
    (newTextsObject, [key, value]) => ({
      ...newTextsObject,
      [key]: replaceValue(value, keys),
    }),
    {}
  );

const replaceValue = <T>(
  value: T,
  keys?: Record<string, string>
): T | string | object => {
  if (isString(value)) return replaceText(value, keys);
  if (isObject(value)) return initializeTexts(value, keys);
  if (isArray(value)) return replaceValues(value, keys);

  return value;
};

const replaceValues = <T extends any[]>(
  values: T,
  keys?: Record<string, string>
) => values.map((value) => replaceValue(value, keys));

export default initializeTexts;
