import initializeTexts from './initializeTexts';

test('returns the same text, if there are no texts with keys', () => {
  const text = 'Hello World';

  const result = initializeTexts({ text }, { name: 'Albert' });

  expect(result).toEqual({ text });
});

test('returns the same text, if there are no keys', () => {
  const text = 'Hello ';

  const result = initializeTexts({ text });

  expect(result).toEqual({ text });
});

test('replaces specific key', () => {
  const text = 'Hello $name!';
  const name = 'Albert';

  const result = initializeTexts({ text }, { name });

  expect(result).toEqual({ text: 'Hello Albert!' });
});

test('replaces multiple occurences of a specific key', () => {
  const text = 'Hello $name! Is $name actually your name?';
  const name = 'Albert';

  const result = initializeTexts({ text }, { name });

  expect(result).toEqual({
    text: 'Hello Albert! Is Albert actually your name?',
  });
});

test('replaces multiple properties', () => {
  const text = 'Hello $name!';
  const moreText = 'Is $name actually your name?';
  const name = 'Albert';

  const result = initializeTexts({ text, moreText }, { name });

  expect(result).toEqual({
    text: 'Hello Albert!',
    moreText: 'Is Albert actually your name?',
  });
});

test('replaces deep properties', () => {
  const text = 'Hello $name!';
  const moreText = 'Is $name actually your name?';
  const name = 'Albert';

  const result = initializeTexts({ text, content: { moreText } }, { name });

  expect(result).toEqual({
    text: 'Hello Albert!',
    content: {
      moreText: 'Is Albert actually your name?',
    },
  });
});

test('replaces array properties', () => {
  const text = 'Hello $name!';
  const moreText = 'Is $name actually your name?';
  const name = 'Albert';

  const result = initializeTexts({ text, content: [moreText] }, { name });

  expect(result).toEqual({
    text: 'Hello Albert!',
    content: ['Is Albert actually your name?'],
  });
});

test('does not replace keys if escaped', () => {
  const text = 'Hello $name! Is _$name actually your name?';
  const name = 'Albert';

  const result = initializeTexts({ text }, { name });

  expect(result).toEqual({
    text: 'Hello Albert! Is $name actually your name?',
  });
});

test('replaces multiple specific keys', () => {
  const text = 'Hello $name! My name is $secondName.';
  const name = 'Albert';
  const secondName = 'Thomas';

  const result = initializeTexts({ text }, { name, secondName });

  expect(result).toEqual({ text: 'Hello Albert! My name is Thomas.' });
});
