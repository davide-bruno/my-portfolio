const getIdMapEntries = <ITEM extends Record<string, any>, IDS extends string>(
  idMap: IdMap<ITEM, IDS>
) => Object.entries(idMap) as [IDS, ITEM][];

const idMapToArray = <ITEM extends Record<string, any>, IDS extends string>(
  idMap: Record<IDS, ItemWithoutId<ITEM>>
) =>
  getIdMapEntries(idMap).reduce<ItemWithId<ITEM, IDS>[]>(
    (array, [id, item]) => [...array, { id, ...item }],
    []
  );

type IdMap<ITEM extends Record<string, any>, IDS extends string> = Record<
  IDS,
  ItemWithoutId<ITEM>
>;

type ItemWithoutId<ITEM extends Record<string, any>> = {
  [P in keyof ITEM]: P extends IdKey ? never : ITEM[P];
};

type ItemWithId<ITEM extends Record<string, any>, IDS extends string> = {
  [K in ItemWithIdKeys<ITEM>]: K extends keyof ITEM
    ? ITEM[K]
    : K extends IdKey
    ? IDS
    : never;
};

type ItemWithIdKeys<ITEM extends Record<string, any>> = keyof ITEM | IdKey;
type IdKey = 'id';

export default idMapToArray;
