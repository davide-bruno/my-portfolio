import idMapToArray from './idMapToArray';

test('returns an array', () => {
  const result = idMapToArray({});
  expect(Array.isArray(result)).toBe(true);
});

test('returns an array with id properties for every item', () => {
  const itemOne = { a: 0 };
  const itemTwo = { a: 5 };
  const itemThree = { a: 13 };
  const idMap = { itemOne, itemTwo, itemThree };

  const [resultOne, resultTwo, resultThree] = idMapToArray(idMap);

  expect(resultOne).toEqual({ id: 'itemOne', a: 0 });
  expect(resultTwo).toEqual({ id: 'itemTwo', a: 5 });
  expect(resultThree).toEqual({ id: 'itemThree', a: 13 });
});
